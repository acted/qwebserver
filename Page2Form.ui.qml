import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id:page2
    title: qsTr("Page 2")

    Text {
        width: page2.width
        height: page2.height
        text: "由经常咕咕的Acted咕咕喵制作\n抉择之路程序版本：Ver抉择之路数据版本：Data服务器版本：2.0\n抉择之路网页版本：https://acted.gitlab.io/h2\n抉择之路源代码https://www.gitlab.com/acted/h2"
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        anchors.centerIn: parent
    }
}
