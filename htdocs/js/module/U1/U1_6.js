export function clsup_e(){
    clsshow(
        "柜台调情",
    '<p>收银员让到了一旁，莉娅代替他站到了柜台前，她想看看你选了些什么。</p><p>“我发现昨晚的事给了你一些很棒的想法。很棒嘛，你需要一些练习。这样，等下你来我家，我给你一些指导如何？”</p><p>考虑到你已经决定要沿着这条路走下去了，你还能说不吗？</p><p>你同意等会就去她家</p><p>1,2,3-去莉娅家（alpha-1）</p><p>4,5,6-先回家一趟（alpha+1）</p><p>[可选择投掷骰子]<br></p>',
    0,
    "U1/U1_6",
    //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_6\',\'go\')">去莉娅家</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_6\',\'home\')">先回家一趟</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="random()">使用骰子</a>'
    [{"btnfun":"clsfin(\'U1/U1_6\',\'go\')","btnname":"去莉娅家"},{"btnfun":"clsfin(\'U1/U1_6\',\'home\')","btnname":"先回家一趟"},{"btnfun":"random()","btnname":"使用骰子","noclose":true}]
    );
}
export function clsfin_e(info){
    switch(info){
        case "home":{
            alpha(-1);
            localStorage.setItem("cls","U1_8");
            break;
        }
        case "go":{
            alpha(1);
            localStorage.setItem("cls","U1_9");
            break;
        }
        default:{
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
    /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
    app.initia();
}


/* 
    '<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_6\',\'home\')">你正在喝果汁（伪娘值+1）</a>
    <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_6\',\'go\')">你正在喝啤酒（母狗值+1）</a>*/