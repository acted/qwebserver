//红丝带判断buff-judge来确定
export function clsup_e() {
    if(localStorage.getItem("buff-judge")){
        var num = localStorage.getItem("buff-judge").split("/meow/").length;
        localStorage.removeItem("buff-judge");
    }else{
        var num=0
    }
    clsshow(
        "裁判",
        '<p>在后院里，你看到一群人正在聚集在颁奖台旁。</p><p>“你和多少裁判做爱了？”，你身边的一个女孩问你，看着你困惑的眼神，她继续解释，“那些右臂上有红色臂环的人，你如果没和他们做爱恐怕得不到奖。很不公平吧。”</p><p>-和0或1个裁判做过爱：参与奖<br>-和2个裁判做爱过：二等奖<br>-和3个裁判做爱过：一等奖<br>（系统自动评判）</p>',
        0,
        "U2/U2_e0",
        //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_e0\',\'' + num + '\')">领奖</a>'
        [{"btnfun":"clsfin(\'U2/U2_e0\',\'" + num + "\')","btnname":"领奖"}]
    )
}

export function clsfin_e(info) {
    alpha(1)
    switch (info) {
        case "0": {
            clsshow(
                "裁判",
                '<p>智慧系统认为你和0个裁判做爱过，参与奖<br>奖励为：alpha+1，手交5分钟</p>',
                0,
                "U2/U2_e0",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_e0\',\'done\')">领奖</a>'
                [{"btnfun":"clsfin(\'U2/U2_e0\',\'done\')","btnname":"领奖"}]
            )
            break;
        }
        case "1": {
            clsshow(
                "裁判",
                '<p>智慧系统认为你和1个裁判做爱过，参与奖<br>奖励为：alpha+1，手交5分钟</p>',
                0,
                "U2/U2_e0",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_e0\',\'done\')">领奖</a>'
                [{"btnfun":"clsfin(\'U2/U2_e0\',\'done\')","btnname":"领奖"}]
            )
            break;
        }
        case "2": {
            slut(1);
            money(25)
            clsshow(
                "裁判",
                '<p>智慧系统认为你和2个裁判做爱过，二等奖<br>奖励为：alpha+1，slut+1，money+25，乳交10分钟（自己奖赏自己吧）</p>',
                0,
                "U2/U2_e0",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_e0\',\'done\')">领奖</a>'
                [{"btnfun":"clsfin(\'U2/U2_e0\',\'done\')","btnname":"领奖"}]
            )
            break;
        }
        case "3": {
            slut(2);
            money(50)
            clsshow(
                "裁判",
                '<p>智慧系统认为你和3个裁判做爱过，一等奖<br>奖励为：alpha+1，slut21，money+50，口交15分钟（自己奖赏自己吧）</p>',
                0,
                "U2/U2_e0",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_e0\',\'done\')">领奖</a>'
                [{"btnfun":"clsfin(\'U2/U2_e0\',\'done\')","btnname":"领奖"}]
            )
            break;
        }
        case "done": {
            localStorage.removeItem("buff-party")
            localStorage.removeItem("buff-U2")
            if (localStorage.getItem("buff-find")) {//找到没
                localStorage.removeItem("buff-find")
                localStorage.setItem("cls", "U2_e1");
                /*dialogAlert("本关卡完成", "成功");*/
                ClsPicOff();
                app.initia();
            } else {
                localStorage.setItem("cls", "U2_e2");
                /*dialogAlert("本关卡完成", "成功");*/
                ClsPicOff();
                app.initia();
            }
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题")
            break;
        }
    }
}