export function clsup_e() {
    clsshow(
        "清晨散步",
        '<p>你抓起昨晚穿的衣服，步行前往你女朋友的公寓。</p><p>现在是星期一的早上，人们已经起床了，然而去上班还是早了点。</p><p>你加快了脚步，你并不想被别人看到穿着这身衣服。你仍然想着昨晚的事情：多么棒的派对啊！但你还是希望短期内不要再参加一次了。</p><p>被女朋友日是一回事，被陌生人日是另一回事。</p><br><p>-1,2,3-走小路（sissy+2）</p><p>-4,5,6-走大路（alpha+4）</p>',
        0,
        "U3/U3_5",
        //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_5\',\'small\')">走小路</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_5\',\'big\')">走大路</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="random()">使用骰子</a>'
        [{ "btnfun": "clsfin(\'U3/U3_5\',\'big\')", "btnname": "走大路" }, { "btnfun": "clsfin(\'U3/U3_5\',\'small\')", "btnname": "走小路" }, { "btnfun": "random()", "btnname": "使用骰子", "noclose": true }]
    );
}
export function clsfin_e(info) {
    switch (info) {
        case "small": {//走小路
            sissy(2);
            clsshow("清晨散步",
                '<p>女装穿戴整齐，最好是早上（什么时间随便啦），走小路30分钟</p>',
                0,
                "U3/U3_5",
                //'<a href="#!" class=" modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_5\',\'e\')">完成</a>'
                [{ "btnfun": "clsfin(\'U3/U3_5\',\'e\')", "btnname": "完成" }]
            )
            break;
        }
        case "big": {//走大路
            alpha(4);
            clsshow("清晨散步",
                '<p>女装穿戴整齐，最好是早上（什么时间随便啦），走大路15分钟</p>',
                0,
                "U3/U3_5",
                //'<a href="#!" class=" modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_5\',\'e\')">完成</a>'
                [{ "btnfun": "clsfin(\'U3/U3_5\',\'e\')", "btnname": "完成" }]
            );
            break;
        }
        case "e": {
            localStorage.setItem("cls", "U3_7");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题")
        }
    }
}