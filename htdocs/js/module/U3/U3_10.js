export function clsup_e() {
    clsshow(
        "贞操锁",
        '<p>在她穿好衣服要去上班时，她继续和你聊了起来。</p><p>“嗯，今天是星期一，你不需要去上班吗？或者，你想找一份更适合你的工作？来，这里是一些上周五的报纸，上面肯定有你需要的东西。我得走快点了，晚上见。”</p><p>她亲了亲你的脸颊然后走开了。</p><br><p>-找一份新工作（sissy+2）</p><p>-回到原来的工作（alpha+2）</p><br><p>PS：喵并没有实现其原来的工作系统，也不打算在每话都给300块。因此，之后的每话喵随心给点钱好啦</p><p>可以访问<a href="https://acted.gitlab.io/h3">小游戏</a></p><p>money+1000</p>',
        0,
        "U3/U3_10",
        //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_10\',\'new\')">找一份新工作</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_10\',\'old\')">维持原来的工作</a>'
        [{ "btnfun": "clsfin(\'U3/U3_10\',\'new\')", "btnname": "找一份新工作" }, { "btnfun": "clsfin(\'U3/U3_10\',\'old\')", "btnname": "维持原来的工作" }]
    )
}

export function clsfin_e(info) {
    switch (info) {
        case "new": {
            /*dialogAlert("本关卡完成", "成功");*/
            sissy(2);
            break;
        }
        case "old": {
            alpha(2);
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            return;
        }
    }
    money(1000)
    ClsPicOff();
    app.initia();
    localStorage.setItem("cls", "U3/U3_11");
}