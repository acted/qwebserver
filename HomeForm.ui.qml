import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id:pageh
    title: qsTr("Home")

    Text {
        width: pageh.width
        height: pageh.height
        text: qsTr("这个是抉择之路服务端第二版\n其采用了QML技术，使得界面更好看了。\n在左上角进入“服务器控制”之后，即可打开服务器。\n在“设置”-“开发者模式”中将键“setting-remote”设置为这个网址还可享受不成熟的云端同步功能")
        anchors.verticalCenterOffset: 0
        anchors.horizontalCenterOffset: 0
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
        wrapMode: Text.WordWrap
    }
}
