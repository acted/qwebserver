#ifndef H2SERVER_H
#define H2SERVER_H

#include <QObject>
#include"JQHttpServer.h"
#include <QtCore>
#include<QFile>
#include <QMimeDatabase>
#include<QMap>

class H2server: public QObject
{
    Q_OBJECT

    Q_PROPERTY( QString ips READ getHostIpAddress)
public:
    H2server();
    QString getHostIpAddress();

private:
    JQHttpServer::TcpServerManage *TCPsm;
    bool isRunning=false;
    void StartServer();
    void StopServer();

public slots:
    void changeServer();

signals:
    void serverChanged();
    void serverRunning();
    void serverStoped();

};

#endif // H2SERVER_H
