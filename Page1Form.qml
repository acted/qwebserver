import QtQuick 2.12
import QtQuick.Controls 2.5
import H2server 1.0

Page {
    id: page1
    property alias button1: button1

    title: qsTr("Page 1")

    Label {
        id: serverstatus
        text: qsTr("当前服务器状态：关闭")
        anchors.verticalCenterOffset: -58
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    H2server{
        id:h2s
        onServerRunning:{
            serverstatus.text=qsTr("当前服务器状态：启动\n访问http://"+h2s.ips+":23412即可")
        }
        onServerStoped: serverstatus.text=qsTr("当前服务器状态：关闭")

    }
    Button {
        id: button1
        text: qsTr("切换状态")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        onClicked:h2s.changeServer()
    }
}

/*##^##
Designer {
    D{i:1;anchors_height:38;anchors_width:130}
}
##^##*/
