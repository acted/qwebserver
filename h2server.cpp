#include "h2server.h"
#include<QDebug>
#include<QString>
#include<QNetworkInterface>

H2server::H2server()
{
    return;
}


void H2server::StartServer(){
    TCPsm=new JQHttpServer::TcpServerManage(10);

    TCPsm->setHttpAcceptedCallback( []( const QPointer< JQHttpServer::Session > &session )
    {
        // 回调发生在新的线程内，不是主线程，请注意线程安全
        // 若阻塞了此回调，那么新的连接将不会得到处理（默认情况下有2个线程可以阻塞2次，第3个连接将不会被处理）

//        session->replyText( QString( "url:%1\nbody:%2" ).arg( session->requestUrl(), QString( session->requestBody() ) ) );
//        session->replyRedirects( QUrl( "http://www.baidu.com" ) );
//        session->replyJsonObject( {         { { "message", "ok" } ,{"happy","yes"} }     } );
//        session->replyJsonArray( { "a", "b", "c" } );
//        session->replyFile( "/Users/jason/Desktop/Test1.Test2" );
//        session->replyImage( QImage( "/Users/jason/Desktop/Test.png" ) );
//        session->replyBytes(QByteArray(4,'\x24')); // $$$$

        // 注1：因为一个session对应一个单一的HTTP请求，所以session只能reply一次
        // 注2：在reply后，session的生命周期不可控，所以reply后不要再调用session的接口了
        QFile fi;
        QString url;
        QMimeDatabase db;

        if(session->requestUrlPath()=="")
#ifdef Q_OS_ANDROID
            url = "assets:/htdocs/index.html";
#else
            url = "./htdocs/index.html";
#endif

        else if(session->requestUrlPath()=="/save"){
            qDebug()<<"saved for "<<session->requestPost()["\"datas\""];
            fi.setFileName("./data.json");
            fi.open(QFile::WriteOnly);
            fi.write(session->requestPost()["\"datas\""]);
            session->replyText("",204);
            fi.close();
            return;
        }else if(session->requestUrlPath()=="/get"){
            qDebug()<<"reply backup";
            fi.setFileName("./data.json");
            fi.open(QFile::ReadOnly);
            QByteArray jsons=fi.readAll();
            if(session->requestPost()["\"datas\""]!=jsons)
                session->replyBytes(jsons,200,"application/json;charset=UTF-8");
            else
                session->replyText("",204);
            fi.close();
            return;
        }else
#ifdef Q_OS_ANDROID
            url= "assets:/htdocs"+session->requestUrlPath();
#else
            url= "./htdocs"+session->requestUrlPath();
#endif


        fi.setFileName(url);
        if(!fi.open(QFile::ReadOnly)){
            qDebug()<<url<<fi.errorString();
            session->replyText(QString("Error!%1:%2").arg(fi.fileName(),fi.errorString()));
            return;
        }
        /*if(db.mimeTypeForFile(url).inherits("text/html")){
            qDebug()<<"replyHtml"<<url<<db.mimeTypeForFile(url).name();
            session->replyHtml(QString(fi.readAll()));
        }else if(db.mimeTypeForFile(url).inherits("application/javascript")){
            qDebug()<<"replyJavaScrpit"<<url<<db.mimeTypeForFile(url).name();
            session->replyJavaScript(QString(fi.readAll()));
        }else if(db.mimeTypeForFile(url).inherits("image/jpeg") || db.mimeTypeForFile(url).inherits("image/png")){
            qDebug()<<"replyImage"<<url<<db.mimeTypeForFile(url).name();
            session->replyImage(QImage(url));
        }else{*/
            qDebug()<<"replyBytes"<<url<<db.mimeTypeForFile(url).name();
            session->replyBytes(fi.readAll(),200,db.mimeTypeForFile(url).name());
        //}
        fi.close();
    } );

    qDebug() << "listen:" << TCPsm->listen( QHostAddress::Any, 23412 );
}

void H2server::StopServer(){
    delete TCPsm;
}

void H2server::changeServer()
{
    if(isRunning){
         qDebug()<<"stop";
         StopServer();
         isRunning=false;
         emit(serverStoped());
    }
    else{
        qDebug()<<"start";
        StartServer();
        isRunning=true;
        emit(serverRunning());
    }
    emit(serverChanged());
}

QString H2server::getHostIpAddress()
{
    QString strIpAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // 获取第一个本主机的IPv4地址
    int nListSize = ipAddressesList.size();
    for (int i = 0; i < nListSize; ++i)
    {
           if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
               ipAddressesList.at(i).toIPv4Address()) {
               strIpAddress = ipAddressesList.at(i).toString();
               break;
           }
     }
     // 如果没有找到，则以本地IP地址为IP
     if (strIpAddress.isEmpty())
        strIpAddress = QHostAddress(QHostAddress::LocalHost).toString();
     return strIpAddress;
}

